//
//  Stack.cpp
//  Homework_5
//
//  Created by Ahmad Ismail on 10/14/13.
//  Copyright (c) 2013 Ahmad Ismail. All rights reserved.
//

#include "Stack.h"

Stack::Stack(){
    head = NULL;
    tail = NULL;

}
Stack::~Stack(){
    
}

StackNode Stack::top(){     // returns top stacknode
    try{
        if(tail == NULL)
        {
            throw exception();
        }
        else
                return *tail;
    }
    catch(exception)
    {
        cout << "This is a empty stack";
    }
}

void Stack::pop(){          // removes top stacknode
    try{
        if(head == NULL && tail == NULL)
        {
            throw exception();
        }
        else if (tail == head)
        {
            tail = NULL;
        }else
            tail = tail -> prev;

    }
    catch(exception)
    {
        cout << "This is a empty stack";
        return;
    }
    return;
}

void Stack::push(StackNode *push){ // adds stacknode to top of stack
    if(tail == NULL){
        tail = push;
        head = push;
    }
    else{
        tail -> next = push;
        push -> prev = tail;
        tail = push;
    }
    
}