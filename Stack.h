//
//  Stack.h
//  Homework_5
//
//  Created by Ahmad Ismail on 10/14/13.
//  Copyright (c) 2013 Ahmad Ismail. All rights reserved.
//

#ifndef __Homework_5__Stack__
#define __Homework_5__Stack__

#include "StackNode.h"
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Stack{
public:
    Stack();
    ~Stack();
    StackNode top();
    void pop();
    void push(StackNode*);
    StackNode *head, *tail;

};
#endif /* defined(__Homework_5__Stack__) */
