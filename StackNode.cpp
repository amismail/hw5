//
//  StackNode.cpp
//  Homework_5
//
//  Created by Ahmad Ismail on 10/14/13.
//  Copyright (c) 2013 Ahmad Ismail. All rights reserved.
//

#include "StackNode.h"
StackNode::StackNode()
{
    identifier = 0;
	next=prev= NULL;
}

StackNode::StackNode(mathVector *d){
    
    vector = d;
    identifier = 1;         //When identifier = 1, then stacknode holds a mathVector used for testing through the stack
}

StackNode::StackNode(char c){
    
    sign = c;
    identifier = 2;         //When identifier = 2, stacknode holds a char. AKA '[' '(' used for testing through the stack
}


StackNode::~StackNode()
{
    
}

int StackNode::getId(){
    return identifier;
}

char StackNode::getChar(){
    return sign;
}

mathVector StackNode::getVector(){
    return *vector;
}