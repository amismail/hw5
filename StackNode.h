//
//  StackNode.h
//  Homework_5
//
//  Created by Ahmad Ismail on 10/14/13.
//  Copyright (c) 2013 Ahmad Ismail. All rights reserved.
//

#ifndef __Homework_5__StackNode__
#define __Homework_5__StackNode__

#include <iostream>
#include "mathVector.h"
class StackNode
{
public:
	StackNode();
    StackNode(mathVector *);
    StackNode(char);
	~StackNode();
    
    
    mathVector *vector;
    char sign;
    int identifier;
    StackNode *next, *prev;
    
    int getId();
    char getChar();
    mathVector getVector();
};

#endif /* defined(__Homework_5__StackNode__) */
