//
//  main.cpp
//  Homework_5
//
//  Created by Ahmad Ismail on 10/9/13.
//  Copyright (c) 2013 Ahmad Ismail. All rights reserved.
//

#include <iostream>
#include <string>
#include "mathVector.h"
#include "Stack.h"
using namespace std;

void populateCalc(string, Stack &);

int main(int argc, const char * argv[])
{
    Stack stack = *new Stack();
    //string temp = "(((([1,2,3]+[2,3,4])-[3,4,5])+[1,2,3])*[3])";
    string temp;
    
    cout << "Please enter the equation you would like to solve."<< endl;
    cout << "Make Sure To Omit any spaces, as it will throw an exception" << endl;
    cin >> temp;
    populateCalc(temp, stack);
    cout << stack.top().getVector().toString() << endl;

    return 0;
}

bool sanityCheck( string command){          //Checks to see if the input string is able to be calculated
    int pStart = 0;                         //Counts how many beginning parentheses there are '('
    int pEnd = 0;                           //Counts closing parentheses. MUST BE EQUAL TO pStart
    int bStart = 0;                         //Same logic as parentheses
    int bEnd = 0;
    int signs =0;                           //Counts # of arithmetic functions. MUST be equal to # of parentheses
    for(int i =0; i < command.length() ; i++){
        switch(command[i]){
            case '[': bStart ++;
                break;
            case ']': bEnd ++;
                break;
            case '(': pStart++;
                break;
            case ')': pEnd++;
                break;
            case '+':
            case '-':
            case '*': signs++;
                break;
            case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9' : case '0': case ',':
                break;
            default: return false;          //If not a parentheses, bracket, sign, number, or comma, then you throw exception
        }
    }
    if( pStart == pEnd &&   pStart == bStart-1 && bStart == bEnd && signs == pStart)
        return true;
    else return false;
    
}

void populateCalc(string command, Stack &stack){
    try{                                // if sanity check fails then quit the program
        if(!sanityCheck(command))
        { 
            throw exception();
        }
    else{                               //begin calculating string
        int temp;
        bool flag = false;              //flag tells you that you found the '[' you are looking for ']' to complete your mathVetor
        for(int i = 0; i < command.length(); i++){
            if(command[i] == '[' || command[i] == ']'){
                if(flag){
                    stack.push(new StackNode(new mathVector(command.substr(temp,i-temp+1))));
                    flag = false;
                }
                else{
                    flag = true;
                    temp = i;
                }

            }
            else if (!flag){                    //Find end of parenthesis, so must do arithmetic function
                if(command[i] == ')'){         
                    mathVector temp(stack.top().getVector().toString());    // Top vector
                    stack.pop();
                    if(stack.top().getChar() == '*'){
                        stack.pop();
                        mathVector temp2(stack.top().getVector() * temp); // solves for first vector * second vector
                        stack.pop();
                        stack.pop();
                        stack.push(new StackNode(new mathVector(temp2)));
                    }
                    else if (stack.top().getChar() == '-'){
                        stack.pop();
                        mathVector temp2(stack.top().getVector() - temp); // solves for first vector- second vector
                        stack.pop();
                        stack.pop();
                        stack.push(new StackNode(new mathVector(temp2)));
                    }
                    else if (stack.top().getChar() == '+'){
                        stack.pop();
                        mathVector temp2(stack.top().getVector() + temp); // solve for first vector + second vector
                        stack.pop();
                        stack.pop();
                        stack.push(new StackNode(new mathVector(temp2)));
                    }
                }
                else{
                stack.push(new StackNode(command[i]));
                }
            }

        }//endfor
    }//end else
    }//end try
    
    catch(exception){
            cout << "Your input cannot be read.";
        std::exit(0);
        }
}