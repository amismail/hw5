CPPFLAGS=-std=c++11 -Wall -g 

# -Werror -std=c++11

#  valgrind --tool=memcheck ./memory

CC=g++-4.8 # or g++-4.8



all: main mathVector Stack StackNode



main: main.cpp

	${CC} main.cpp -o $@ ${CPPFLAGS}



mathVector: mathVector.cpp

	${CC} mathVector.cpp -o $@ ${CPPFLAGS}


Stack: Stack.cpp

	${CC} Stack.cpp -o $@ ${CPPFLAGS}



StackNode: StackNode.cpp

	${CC} StackNode.cpp -o $@ ${CPPFLAGS}


.PHONY: clean


clean:

	-@rm *.o main mathVector Stack StackNode 2>/dev/null || true

