//
//  mathVector.cpp
//  Homework_5
//
//  Created by Ahmad Ismail on 10/13/13.
//  Copyright (c) 2013 Ahmad Ismail. All rights reserved.
//

#include "mathVector.h"
mathVector::mathVector(){
    dimension = 0;
    vecArr = nullptr; 
}
mathVector::mathVector(mathVector const& copy){ //creates a math vector and copies certain math vector copies it into it
    dimension = copy.dimension;
    vecArr = new double[dimension];
    for (int i = 0; i < dimension; i++) {
        vecArr[i] = copy.vecArr[i];
    }
}

mathVector::mathVector(double vec1){ //creates one dimensional vector for one variable
    vecArr = new double[1];
    vecArr[0] = vec1;
    dimension = 1;
}
mathVector::mathVector(string vect){ //takes a string and writes it into a vector
    dimension = 0;
    for(int i = 0; i < vect.size(); i++) {
        if(vect[i] == ','){
            dimension++;
        }
    }
    dimension++;
    vecArr = new double[dimension];
    int j=0;
    int asdf = 1;
    for(int i = 0; i < vect.size(); i++) {
        if(vect[i] == ',' || vect[i] == ']'){
            string x = vect.substr(asdf,i);
            asdf = i+1;
            double temp;
            temp=atof(x.c_str());
            vecArr[j]=temp;
            j++;
        }
    }
}

mathVector::~mathVector(){
    delete[] vecArr;
    vecArr = NULL;
}

string mathVector::toString(){ // takes vector and writes it out into a string
    string a ="[";
    string b=",";
    string final;
    ostringstream inyougo;
    inyougo << "["<< vecArr[0];
    for (int i=1; i < dimension; i++)
    {
        inyougo <<","<< vecArr[i];
    }
        final =inyougo.str() + "]";
    
    return final;

}
mathVector mathVector::operator+(const mathVector& w) const{
    
    mathVector addition(*this);
    try{
        if(addition.dimension != w.dimension)
            throw exception();
    }
    catch(exception){
        cout<< "Your dimensions are not equal. Terminating Program.";
        std::exit(0);
    }
    for(int i = 0; i < dimension; i++)
    {
        addition.vecArr[i] += w.vecArr[i];
    }
    return addition;
}
mathVector mathVector::operator-(const mathVector& w) const{
    mathVector subtraction(*this);
    try{
        if(subtraction.dimension != w.dimension)
            throw exception();
    }
    catch(exception){
        cout<< "Your dimensions are not equal. Terminating Program.";
        std::exit(0);

    }
    for(int i = 0; i < dimension; i++)
    {
        subtraction.vecArr[i] -= w.vecArr[i];
    }
    return subtraction;
}
bool mathVector::operator==(const mathVector& w) const{
    if(dimension != w.dimension)
        return false;
    
    for(int i = 0; i < dimension; i++)
    {
        if(vecArr[i] != w.vecArr[i])
            return false;
    }
    return true;
}

bool mathVector::operator!=(const mathVector& w) const{
    if(dimension != w.dimension)
        return true;
    for(int i = 0; i < dimension; i++)
    {
        if(vecArr[i] != w.vecArr[i])
            return true;
    }
    return false;
}

mathVector mathVector::operator*(const mathVector& w) const{
    double multiplication = 0;
    mathVector m(*this);

    try{
        if(dimension != w.dimension && dimension != 1 && w.dimension != 1)
            throw exception();
    }
    catch(exception){
        cout<< "Your dimensions are not equal. Terminating Program.";
        std::exit(0);
    }
    if (dimension == w.dimension){
    for(int i = 0; i < dimension; i++)
    {
        multiplication += vecArr[i]*w.vecArr[i];
    }
        mathVector mult(multiplication);
        return mult;

    }
    else if(w.dimension == 1){
        mathVector temp(m);
        for(int i=0; i<m.dimension; i++){
            temp.vecArr[i] = w.vecArr[0] * m.vecArr[i];
        }
        return temp;
    }    else if(m.dimension == 1){
        mathVector temp(w);
        for(int i=0; i<w.dimension; i++){
            temp.vecArr[i] = m.vecArr[0] * w.vecArr[i];
        }
        return temp;
    }
    return 0;
}
void mathVector::operator=(const mathVector& w){
    dimension = w.dimension;
    try{
        if(dimension != w.dimension)
            throw exception();
    }
    catch(exception){
        cout<< "Your dimensions are not equal. Terminating Program.";
        std::exit(0);
    }
    for(int i = 0; i < dimension; i++)
    {
        vecArr[i] = w.vecArr[i];
    }
    
}
double mathVector::operator[](const int idx) const{
    return vecArr[idx];
}