//
//  mathVector.h
//  Homework_5
//
//  Created by Ahmad Ismail on 10/13/13.
//  Copyright (c) 2013 Ahmad Ismail. All rights reserved.
//

#ifndef __Homework_5__mathVector__
#define __Homework_5__mathVector__

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class mathVector{
public:
    int dimension;
    double* vecArr;
    mathVector(mathVector const &copy);
    mathVector(double vec1);
    mathVector(string vect);
    mathVector();
    ~mathVector();
    string toString();
    mathVector operator+(const mathVector& w) const;
    mathVector operator-(const mathVector& w) const;
    bool operator==(const mathVector& w) const;
    bool operator!=(const mathVector& w) const;
    mathVector operator*(const mathVector& w) const;
    void operator=(const mathVector& w);
    double operator[](const int w) const;
};

#endif /* defined(__Homework_5__mathVector__) */
